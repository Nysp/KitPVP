package ru.litegamemc.kitpvp.kits.utils;

import java.util.LinkedList;

public class KitUtils
{
  private static final LinkedList<Kit> list = new LinkedList<>();
  
  public static Kit get(final String kit)
  {
    for (final Kit kits : list)
    {
      if (kits.getName().equals(kit))
        return kits;
    }
    return list.getFirst();
  }
  
  public static LinkedList<Kit> list()
  {
    return list;
  }
}