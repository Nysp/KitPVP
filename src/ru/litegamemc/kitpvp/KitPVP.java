package ru.litegamemc.kitpvp;

import org.bukkit.plugin.java.JavaPlugin;
import ru.litegamemc.kitpvp.kits.*;
import ru.litegamemc.kitpvp.kits.utils.Kit;
import ru.litegamemc.kitpvp.kits.utils.KitUtils;
import ru.litegamemc.creative.mode.utils.ModeUtils;
import java.util.LinkedList;

public class KitPVP extends JavaPlugin
{
  @Override
  public void onEnable()
  {
    final LinkedList<Kit> list = KitUtils.list();
    list.add(new WarriorKit());
    list.add(new ArcherKit());
    list.add(new BerserkerKit());
    list.add(new KnightKit());
    list.add(new DefenderKit());
    list.add(new CactusKit());
    list.add(new DarkArcherKit());
    ModeUtils.list().add(new KitMode());
  }
  
  @Override
  public void onDisable()
  {
    KitUtils.list().clear();
  }
}