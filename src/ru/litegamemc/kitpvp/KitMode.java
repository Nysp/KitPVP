package ru.litegamemc.kitpvp;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.*;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerLocaleChangeEvent;
import ru.litegamemc.core.utils.*;
import ru.litegamemc.kitpvp.kits.utils.KitMenu;
import ru.litegamemc.creative.menu.WorldSettingsMenu;
import ru.litegamemc.creative.menu.WorldsMenu;
import ru.litegamemc.creative.mode.utils.Mode;
import java.util.LinkedHashMap;
import java.util.UUID;

public class KitMode implements Mode
{
  @Override
  public void loadPlayer(final Player player, final World world)
  {
    player.setGameMode(GameMode.SPECTATOR);
    player.sendTitle("", LocaleUtils.get(player).get("title.clickToContinue"), 0, 100, 5);
  }
  
  @Override
  public void loadWorld(final World world)
  {
    world.setAutoSave(false);
    world.setPVP(true);
  }
  
  @Override
  public void onPlayerInteract(final PlayerInteractEvent event)
  {
    final Player player = event.getPlayer();
    event.setUseInteractedBlock(Event.Result.DENY);
    if (player.getGameMode() != GameMode.SPECTATOR)
    {
      switch (event.getMaterial())
      {
        case FEATHER:
        {
          if (PlayersUtils.isUseItem(event.getAction()))
          {
            event.setUseItemInHand(Event.Result.DENY);
            if (!player.hasCooldown(Material.FEATHER))
            {
              player.setCooldown(Material.FEATHER, 30);
              player.openInventory(new WorldsMenu(LocaleUtils.get(player)).getInventory());
            }
          }
          break;
        }
        case COMPASS:
        {
          if (PlayersUtils.isUseItem(event.getAction()))
          {
            event.setUseItemInHand(Event.Result.DENY);
            if (!player.hasCooldown(Material.COMPASS))
            {
              event.setUseInteractedBlock(Event.Result.DENY);
              player.setCooldown(Material.COMPASS, 30);
              player.openInventory(new WorldSettingsMenu.Quest(LocaleUtils.get(player)).getInventory());
            }
          }
          break;
        }
      }
    }
    else
      player.openInventory(new KitMenu(player.getLevel(), LocaleUtils.get(player)).getInventory());
  }
  
  @Override
  public void onPlayerBucketEmpty(PlayerBucketEmptyEvent event)
  {
    event.setCancelled(true);
  }
  
  @Override
  public void onEntityDamage(final EntityDamageEvent event)
  {
    switch (event.getCause())
    {
      case ENTITY_ATTACK:
      {
        final LinkedHashMap<UUID, UUID> teams = Teams.getTeams();
        final UUID team = teams.get(event.getEntity().getUniqueId());
        if (team != null && team.equals(teams.get(((EntityDamageByEntityEvent) event).getDamager().getUniqueId())))
          event.setCancelled(true);
        break;
      }
      case VOID:
      {
        event.setDamage(20.0);
        break;
      }
    }
  }
  
  @Override
  public void onPlayerDeath(final PlayerDeathEvent event)
  {
    final Player player = event.getEntity();
    player.setNoDamageTicks(50);
    player.teleport(player.getWorld().getSpawnLocation());
  }
  
  @Override
  public void onProjectileLaunch(ProjectileLaunchEvent event)
  {
  
  }
  
  @Override
  public void onProjectileHit(ProjectileHitEvent event)
  {
    event.getEntity().remove();
  }
  
  @Override
  public void onPlayerLocaleChange(final PlayerLocaleChangeEvent event)
  {
    final Player player = event.getPlayer();
    if (player.getGameMode() == GameMode.SPECTATOR)
      player.sendTitle("", LocaleUtils.get(player).get("title.clickToContinue"), 0, 100, 5);
  }
  
  @Override
  public void unloadWorld(final World world)
  {
  
  }
  
  @Override
  public void unloadPlayer(final Player player, final World world)
  {
  
  }
  
  @Override
  public void tickSecond(final World world)
  {
    final String online = String.valueOf(world.getPlayers().size());
    for (final Player players : world.getPlayers())
      players.sendActionBar(LocaleUtils.get(players).get("actionbar.online").replace("{online}", online));
    world.spawnParticle(Particle.VILLAGER_HAPPY, world.getSpawnLocation(), 8, 0.5, 0.5, 0.5);
  }
  
  @Override
  public String getName()
  {
    return "KitPVP";
  }
  
  @Override
  public Material getIcon()
  {
    return Material.IRON_SWORD;
  }
  
  @Override
  public boolean getPlayerCanStart()
  {
    return true;
  }
  
  @Override
  public boolean getGuestsNeedAuth()
  {
    return true;
  }
}